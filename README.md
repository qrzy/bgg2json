<div style="text-align: center">
  <p style="font-size: 3rem; margin-bottom: 0; text-decoration: underline">DEPRECATED</p>
  <p>
    This package has been deprecated.<br/><br/>
    It's been versioned really badly and also missed some important points so now this repository served as a base for new package: <a href="https://www.npmjs.com/package/bgg-xml-api-client">bgg-xml-api-client</a>.
  </p>
</div>

[![Coverage Status](https://coveralls.io/repos/gitlab/qrzy/bgg2json/badge.svg?branch=master)](https://coveralls.io/gitlab/qrzy/bgg2json?branch=master)

# BGG2JSON

It's a simple library providing just a single function that returns requested BGG data as a JavaScript object.
It uses [axios](https://github.com/axios/axios) under the hood, so the return value is wrapped with `AxiosResponse` - just to provide all the data about the response from API.
The main data sits in `data` property of that response object.

## Example usage:

```js
import bgg2json from 'bgg2json';

const { data } = await bgg2json('user', { name: 'Qrzy88' });

console.log(data.id); // displays: 1381959
```

`bgg2json` takes 2 parameters:
- BGG API resource name
- resource parameters as object - for better DX the parameters are typed, but the type is a union of types given to the wrappers listed below

## Wrappers

There are also wrappers available for certain resources that accept params (already typed) as the only argument:

- `getBggCollection(params)`
- `getBggFamily(params)`
- `getBggForum(params)`
- `getBggForumlist(params)`
- `getBggGeeklist(params)`
- `getBggGuild(params)`
- `getBggHot(params)`
- `getBggPlays(params)`
- `getBggSearch(params)`
- `getBggThing(params)`
- `getBggThread(params)`
- `getBggUser(params)`
