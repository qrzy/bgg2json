/* istanbul ignore file */

import bgg2json from './client';

export default bgg2json;

export { bgg2json } from './client';
export { ResourceName } from './types';
export * from './wrappers';
